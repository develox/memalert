use std::io::Error;

use systemstat::{saturating_sub_bytes, Platform, System};

pub fn get_memory_usage() -> Result<u8, Error> {
    let sys = System::new();
    match sys.memory() {
        Ok(mem) => {
            let mem_info = mem.platform_memory.meminfo;

            let mem_used =
                saturating_sub_bytes(mem_info["MemTotal"], mem_info["MemAvailable"]).as_u64();

            let mem_usage =
                (mem_used as f64 * 100.0 / mem_info["MemTotal"].as_u64() as f64).round() as u8;

            Ok(mem_usage)
        }
        Err(e) => Err(e),
    }
}
