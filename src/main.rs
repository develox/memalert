use std::env;
use std::time::Duration;

use notify_rust::Notification;

const INTERVAL: u64 = 1000; // ms

const DEFAULT_THRESHOLD: u8 = 95; // percentage

fn main() {
    let mut threshold = 0;

    let args: Vec<String> = env::args().collect();

    if args.len() > 1 {
        match args[1].parse::<u8>() {
            Ok(n) => {
                if (50..100).contains(&n) {
                    threshold = n;
                } else {
                    println!("Invalid threshold value: {}, must be [50 ~ 99]", n);
                }
            }
            Err(_e) => println!("Invalid threshold value: {}", args[1]),
        }
    }

    if threshold > 0 {
        println!("Alert threshold: {}%", threshold);
    } else {
        threshold = DEFAULT_THRESHOLD;
        println!("Using default alert threshold: {}%", threshold);
    }

    let mut previous_usage = 0;

    loop {
        previous_usage = check_memory_usage(threshold, previous_usage);
        std::thread::sleep(Duration::from_millis(INTERVAL));
    }
}

fn check_memory_usage(threshold: u8, previous_usage: u8) -> u8 {
    match memusage::get_memory_usage() {
        Ok(mem_usage) => {
            if mem_usage != previous_usage {
                let msg = format!("You have used {}% of memory.", mem_usage);
                println!("{}", &msg);

                if mem_usage > threshold {
                    Notification::new()
                        .summary("Memory Alert")
                        .body(&msg)
                        .icon("dialog-warning")
                        .show()
                        .unwrap();
                }
            }

            mem_usage
        }
        Err(e) => panic!("\nMemory: error: {}", e),
    }
}
