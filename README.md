# memalert

memalert checks memory usage every second and shows system notification if it reaches the given threshold.

## Build

```sh
# add MUSL target
rustup target add x86_64-unknown-linux-musl

# build with MUSL target
cargo build --release --target x86_64-unknown-linux-musl
```

\* Dependencies:

libdbus-glib-1-dev might be required for the notification to work, in some Linux distros like Ubuntu.

## Usage

Starts with alert threshold (in percentage, between 50 and 99), for example:

```sh
memalert 90
```

Or, with default threshold (95%):

```sh
memalert
```

Run in background:

```sh
memalert > /dev/null &
```
